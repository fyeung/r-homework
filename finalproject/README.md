
Directory Structure
=====

The project is encapsulated in 1 class with 2 separate helper classes.  

The core class is in Project.R
All methods should easily define the project points in the draft outline.

The script to run is 'main.R' which is the long Script. 
The short Script is omitted because it is just the first part of the 
long script before the 'EXTRA' tag. The script contains examples of 
class usage.

Helper Classes
=======

Per convention, all classes are separated one per file. 

googleClass.R <- Class to talk to the Google Maps API for updating the sites.
calendarHeat.R <- Class to draw the heat map. (Authored by Paul Bleicher) 

How to Run
=======

Unzip the finalproject.zip file. 
Unzip the csv.zip file to ./csv/.
The directory will look like:
/finalproject/
    /lib/
    /images/
    /prototypes/
    /main.R
    /project.R    
/csv/
    AllSites.csv
    ModifiedSites.csv

There are two things you need to change in main.R 
Change the working directory to $PWD/finalproject/ 
(ie. fill in $PWD with your path)
and set the variable 'csv_dir' in main.R  to "../csv/" or whereever the
directory is relative to the project directory.
