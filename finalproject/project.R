# CLASS: -----------------------------------------------------------------
# Project ----------------------------------------------------------------
#'
#' This class is a wrapper for our project.  
#'
#' @section Slots: 
#'  \describe{
#'    \item{\code{slot1}:}{Object of class \code{"data.frame"}, containing the sites data}
#'    \item{\code{slot2}:}{Object of class \code{"data.frame"}, containing the sites meta data}
#'  }
#' @exportClass Project 
#' @author Fuk & Korina 
#' 
setClass("Project", 
  representation(
    sites="data.frame",
    sites_meta="data.frame"
  )
)

# Define the object Generics
# GENERICS: -----------------------------------------------------------------

# gen_lr_plot ---------------------------------------------------------------
setGeneric(
  name="gen_lr_plot", 
  def=function (object, Sites, title) {return(object)}
)

# four_plot ---------------------------------------------------------------
setGeneric(
  name="four_plot", 
  def=function (object) {return(object)}
) 

# heat_map ---------------------------------------------------------------
setGeneric(
  name="heat_map", 
  def=function (object, site, title="2012 Energy Usage") {return(object)}
) 

setGeneric(
  name="bootstrapT", 
  def=function (object, industry, N=10000) {return(object)}
)

setGeneric( 
  name="plotTstar", 
  def=function (object, tstar_bootstrap, industry) {return(object)}
)

setGeneric(
  name="TbootIntervals", 
  def=function (object, tstar_bootstrap, industry) {return(object)}
)
    
# Define the concrete Methods
# METHODS: ------------------------------------------------------------------

#' Initialize (method) -------------------------------------------------
#'
#' This method takes care of the data loading aspects of the project including
#' modifying and cleaning the input data.
#' Initialize runs before the creation of each instance of the object. 
#'
#' @return A Project object with the sites loaded
#' 
setMethod("initialize", "Project", function(.Object, csv_dir="enernoc/csv/"){
  # Set up the meta data first
  sites_meta = read.csv(paste(csv_dir,"ModifiedSites.csv", sep="/"))
  # Make sure we have states and cities or call Google
  if(!is.element("states", names(sites_meta))) {
    # Search Google for an address based on the lat, lng
    print("Wait, Gotta Talk To Google")
    source("lib/googleClass.R")
    Google <- new("GoogleAPI")
    N = nrow(sites_meta)
    cities <- numeric(N)
    states <- numeric(N)
    for(i in 1:N) {
      lat = sites_meta[i,]$LAT; lat
      lng = sites_meta[i,]$LNG; lng
      response <- ReverseGeoLocate(Google, lat, lng)
      if((length(response$results)) == 0) {
        cities[i] = NA 
        states[i] = NA 
        next
      }
      best_address = response$results[[1]]$address_components;
      for(j in 1:length(best_address)) {
        if(is.element("locality", best_address[[j]]$types)) {
          cities[i] = best_address[[j]]$long_name;
        }
        if(is.element("administrative_area_level_1", best_address[[j]]$types)) {
          states[i] = best_address[[j]]$long_name;
        }
      }
    }
    sites_meta["cities"] = cities
    sites_meta["states"] = states
  }
  # This checks for the global variable (which is set in main.R)
  # It is useful since it takes half a min to load the csv.
  if(exists("AllSites")) {
    sites = AllSites
  } else {
    sites = read.csv(paste(csv_dir,"AllSites.csv", sep="/"))
    AllSites = sites
  }
  # Build the combined sites if we haven't already 
  if(nrow(sites) < 50) {
    print("Wait, Gotta Combine all the csv files!")
    sites <- data.frame()
    for(i in 1:100) {
      site_id = sites_meta[i,]$SITE_ID
      site_file = paste(csv_dir, as.character(site_id), ".csv", sep="") 
      # Read the file and append the column as the site_id
      site_x <- read.csv(site_file)
      site_x[,'site_id'] = site_id
      sites <- rbind(sites, site_x)
    }
  }
  
  .Object@sites <- sites
  .Object@sites_meta <- sites_meta 
  callNextMethod()
  return(.Object)
})

#' gen_lr_plot (method) --------------------------------------------------------
#'
#' Generates a Linear Regression plot between SQ_FT and MEAN_USAGE.
#' TODO: Make it more generic
#'
setMethod(
  f="gen_lr_plot", 
  signature="Project", 
  definition=function (object, Sites, title="Plot") {
    plot(Sites$SQ_FT, Sites$MEAN_USAGE, main=title, xlim=c(0, 1500000))
    sites.lm <- lm(Sites$MEAN_USAGE~Sites$SQ_FT, data=Sites)
    a <- as.double(sites.lm$coefficients[1]); a
    b <- as.double(sites.lm$coefficients[2]); b
    abline(a, b, col = "red")
    print(cor(Sites$MEAN_USAGE,Sites$SQ_FT))
    return(sites.lm)
  }
)  

#' four_plot (method) --------------------------------------------------------
#'
setMethod(
  f="four_plot", 
  signature="Project", 
  definition=function (object) {
    par(mfrow=c(2,2)) 
    Sites <- object@sites_meta;
    a = gen_lr_plot(object, Sites[Sites$INDUSTRY=="Commercial Property",], "Commercial Property")
    b = gen_lr_plot(object, Sites[Sites$INDUSTRY=="Education",], "Education")
    c = gen_lr_plot(object, Sites[Sites$INDUSTRY=="Food Sales & Storage",], "Food Sales & Storage")
    d = gen_lr_plot(object, Sites[Sites$INDUSTRY=="Light Industrial",], "Light Industrial")
    return(c(a,b,c,d))
  }
)

#' heat_map (method) --------------------------------------------------------
#' 
setMethod(
  f="heat_map", 
  signature="Project", 
  definition=function (object, site, title) {
    source("lib/calendarHeat.R")
    # Remove anything after 2011 and before 2013
    site = site[site$timestamp > 1325376000,]
    site = site[site$timestamp < 1356998400,]
    site['singleDate'] <- as.Date(as.POSIXct(site$timestamp, origin="1970-01-01"))
    calendarHeat(dates=site$singleDate,
                 values=as.vector(site$value),
                 color="r2gr", 
                 varname=title)
  }
)

setMethod(
  f="bootstrapT", 
  signature="Project", 
  definition=function (object, industry, N) {
    Sites = object@sites_meta;
    industry_sites = Sites[Sites$INDUSTRY==industry,];
    industry_means = industry_sites$MEAN_USAGE
    
    # May or may not contain a long, flat tail depending on industry
    #hist(industry_means, breaks = "FD", main=paste("Means of", industry, "Industry"))
    xbar <- mean(industry_means); xbar  # the sample mean
    S <- sd(industry_means); S          # the sample standard deviation
    n <- length(industry_means); n      # the number of sites in the sample
    SE <- S/(sqrt(n)) ; SE              # the sample standard error
    
    # Estimate the distribution of the t statistic
    # N is provided as a method parameter
    Tstar = numeric(N)                                         # vector of t statistics
    means = numeric(N); StdErrs = numeric(N)                   # Full sample size
    hund = 100; hundMeans = numeric(N); hundStd = numeric(N);  # Small sample size
    thou = 1000; thouMeans = numeric(N); thouStd = numeric(N); # Medium sample size
    for (i in 1:N) {
      x <-sample(industry_means, size = n, replace = TRUE)
      Tstar[i] <-(mean(x) - xbar)/(sd(x)/sqrt(n));   # bootstrap t statistic
      means[i] = mean(x);                            # bootstrap sample mean
      StdErrs[i] = sd(x)/sqrt(n);                    # bootstrap standard error 
      if(i <= 100) {
        hundMeans[i] = means[i];   
        hundStd[i] = StdErrs[i]
      }  
      if(i <= 1000) {
        thouMeans[i] = means[i];   
        thouStd[i] = StdErrs[i]
      }
    }
    plot(means, StdErrs, main="Plot of Means vs StdErr")
    points(thouMeans, thouStd, col="green")
    points(hundMeans, hundStd, col="red")
    return(list(Tstar, means, StdErrs))
  }
)

setMethod(
  f="plotTstar", 
  signature="Project", 
  definition=function (object, tstar_bootstrap, industry) {
    sts = object@sites_meta
    sample = sts[sts$INDUSTRY == industry,]$MEAN_USAGE
    n = length(sample);
    print(n)
    color_dist = "red"
    color_boot = "blue"
    hist(tstar_bootstrap, 
         breaks = "FD", 
         probability = T,
         main=paste("Bootstrap of t-statistic for", industry))
    curve(dt(x,n-1), col = color_dist, add = TRUE) 
    legend("topleft",
           legend=c('t-boot CI', 't-dist CI'),
           lty=c(3, 1),
           lwd=c(3, 1.5),
           col=c(color_boot, color_dist))
    
    # The bootstrap quantiles are very different from the t quantiles
    q<-quantile(tstar_bootstrap, c(.025, .975), names = FALSE); q # will differ slightly from page 198 because it is random and depend on initial conditions
    abline(v=q[1], col=color_boot, lwd=3, lty=3)
    abline(v=q[2], col=color_boot, lwd=3, lty=3)
    qStud <- qt(c(.025, .975), n-1); qStud
    abline(v=qStud[1], col=color_dist, lwd=1.5)
    abline(v=qStud[2], col=color_dist, lwd=1.5)
  }
);
  

setMethod(
  f="TbootIntervals", 
  signature="Project", 
  definition=function (object, tstar_bootstrap, industry) {
    sts = object@sites_meta
    sample = sts[sts$INDUSTRY == industry,]$MEAN_USAGE
    xbar <- mean(sample); xbar  # the sample mean
    S <- sd(sample); S          # the sample standard deviation
    n <- length(sample); n      # the number of sites in the sample
    SE <- S/(sqrt(n)) ; SE              # the sample standard error
  
    q<-quantile(tstar_bootstrap, c(.025, .975), names = FALSE); q 
    # To get a confidence interval, use the bootstrap quantiles 
    # along with the sample mean and standard deviation
    L <- xbar - q[2]*SE; 
    U <- xbar - q[1]*SE; 
    print("T-Bootstrap Confidence interval")
    print(c(L,U))
    
    qStud <- qt(c(.025, .975), n-1); qStud
    L.qt = xbar - qStud[2]*SE 
    U.qt = xbar - qStud[1]*SE
    # Here is the confidence interval based on the assumption of a Student t distribution
    print("T-Distribution Confidence interval")
    print(c(L.qt, U.qt))
    return(c(L,U))
  }
)
