
# There are 100 Sites
Sites <- read.csv("math156//r-homework//meta/all_sites.csv")
nrow(Sites)
head(Sites)

# Example of Grabbing a single site 
dataSiteX = Sites[Sites$SITE_ID == 136,]; dataSiteX
site_id = 136
site_file = paste("math156/r-homework/csv/", as.character(site_id), ".csv", sep=""); 
site_file = paste("enernoc/csv/", as.character(site_id), ".csv", sep=""); 
site_x <- read.csv(site_file)
site_x[,'site_id'] = site_id

# Example of Grabbing 2 sites 
load_single_site = function(site_id) {
  site_info = Sites[Sites$SITE_ID == site_id,]
  print(site_info)
  #site_file = paste("math156/r-homework/csv/", as.character(site_id), ".csv", sep=""); 
  site_file = paste("enernoc//csv/", as.character(site_id), ".csv", sep=""); 
  site_data <- read.csv(site_file)
  site_data[,'site_id'] = site_id
  return(site_data)
}

head(AllSites)
for(i in 1:10) {
  
}

site_idX = 136
site_idY = 51 
site_x = load_single_site(site_idX)
site_y = load_single_site(site_idY)
site_x_usage =  site_x[!is.na(site_x$value),]$value
site_y_usage =  site_y[!is.na(site_y$value),]$value

t.test(site_y, site_x)
t.test(site_y[!is.na(site_y$value),]$value,
       site_x[!is.na(site_y$value),]$value,
       var=T)

# Imagine mean per t-test
Sites
perlocationA = Sites[Sites$TIME_ZONE== 'America/New_York',]$MEAN_USAGE
perlocationB = Sites[Sites$TIME_ZONE== 'America/Los_Angeles',]$MEAN_USAGE
t.test(perlocationA, perlocationB)

hist(Sites$SQ_FT)
quantile(Sites$SQ_FT, c(.25, .75))

perSQftA = Sites[Sites$SQ_FT < 43000,]$MEAN_USAGE
perSQftB = Sites[Sites$SQ_FT > 200000,]$MEAN_USAGE
t.test(perSQftA, perSQftB) 

# Ratio of variances of 2 sites
var.test(site_y[!is.na(site_y$value),]$value,
         site_x[!is.na(site_x$value),]$value)
# The ratio is not 1, so we should use welch's t-test

# We actually can't assume that the variances are the same
t.test(site_y_usage,
       site_x_usage)

# The distribution is actually not normal
hist(site_y$value)
hist(site_x$value)

# Student-T confidence interval 
q = qt(c(.975), df=length(site_x_usage)-1); q
L = mean(site_x_usage) - ((q*sd(site_x_usage))/ sqrt(length(site_x_usage))); L
U = mean(site_x_usage) + ((q*sd(site_x_usage))/ sqrt(length(site_x_usage))); U

# Z-statistic
ztester = function(a, b) {
  n.a = length(a)
  n.b = length(b)
  var.a = var(a)
  var.b = var(b)
  zeta = (mean(a) - mean(b))/(sqrt(var.a/n.a + var.b/n.b))
  return(zeta)
}
zstar = ztester(site_x_usage, site_y_usage); zstar
pnorm(zstar)

# Load up all the csv files and attach a site id for each one
AllSites <- data.frame()
for(i in 1:100) {
  site_id = Sites[i,]$SITE_ID
  site_file = paste("math156/r-homework/csv/", as.character(site_id), ".csv", sep=""); siteFile
  # Read the file and append the column as the site_id
  site_x <- read.csv(site_file)
  site_x[,'site_id'] = site_id
  AllSites <- rbind(AllSites, site_x)
  print(nrow(site_x))
}

nrow(AllSites)
head(AllSites)
head(Sites)

Sites[]

N = nrow(Sites)
mean_usage = numeric(N);
for(i in 1:N) {
  print(i)
  site_id = Sites[i,]$SITE_ID;
  subset = AllSites[AllSites$site_id == site_id,];
  mean_usage[i] = mean(subset$value);
}
Sites['MEAN_USAGE'] = mean_usage;

gen_lr_plot <- function(Sites, title="Plot") {
  plot(Sites$SQ_FT, Sites$MEAN_USAGE, main=title, xlim=c(0, 1500000))
  sites.lm <- lm(Sites$MEAN_USAGE~Sites$SQ_FT, data=Sites)
  a <- as.double(sites.lm$coefficients[1]); a
  b <- as.double(sites.lm$coefficients[2]); b
  abline(a, b, col = "red")
  print(cor(Sites$MEAN_USAGE,Sites$SQ_FT))
}
gen_lr_plot(Sites)
gen_lr_plot(Sites[Sites$SQ_FT<500000,])

par(mfrow=c(2,2)) 
gen_lr_plot(Sites[Sites$INDUSTRY=="Commercial Property",], "Commercial Property")
gen_lr_plot(Sites[Sites$INDUSTRY=="Education",], "Education")
gen_lr_plot(Sites[Sites$INDUSTRY=="Food Sales & Storage",], "Food Sales & Storage")
gen_lr_plot(Sites[Sites$INDUSTRY=="Light Industrial",], "Light Industrial")

table(Sites$states)
tab = table(Sites$TIME_ZONE, Sites$INDUSTRY)
tab
(tab[,name='Commercial Property'])
head(Sites)

# Types of industry are uniform:
# Commercial Property, Education, Food Sales & Storage, Light Industrial
table(Sites$SUB_INDUSTRY, Sites$INDUSTRY) 

table(Sites[Sites$INDUSTRY=="Commercial Property",]$SUB_INDUSTRY)
table(Sites[Sites$INDUSTRY=="Education",]$SUB_INDUSTRY)
table(Sites[Sites$INDUSTRY=="Food Sales & Storage",]$SUB_INDUSTRY)
table(Sites[Sites$INDUSTRY=="Light Industrial",]$SUB_INDUSTRY)

SiteX <- read.csv("math156/r-homework/csv/136.csv")

# Right the file if it does not exist 
# write.csv(AllSites, file="math156/r-homework/csv/AllSites.csv")
AllSites = read.csv("math156/r-homework/csv/AllSites.csv")
head(AllSites)
source("math156/topsy/midterm/classes/googleClass.R")

# Search Google for an address based on the lat, lng
Google <- new("GoogleAPI")

N = nrow(Sites)
cities <- numeric(N)
states <- numeric(N)
for(i in 1:N) {
  lat = Sites[i,]$LAT; lat
  lng = Sites[i,]$LNG; lng
  response <- ReverseGeoLocate(Google, lat, lng)
  if((length(response$results)) == 0) {
      cities[i] = NA 
      states[i] = NA 
      next
  }
  best_address = response$results[[1]]$address_components;
  for(j in 1:length(best_address)) {
    if(is.element("locality", best_address[[j]]$types)) {
      cities[i] = best_address[[j]]$long_name;
    }
    if(is.element("administrative_area_level_1", best_address[[j]]$types)) {
      states[i] = best_address[[j]]$long_name;
    }
  }
}
Sites["cities"] = cities
Sites["states"] = states
head(Sites)
write.csv(Sites, file="math156/r-homework/csv/ModifiedSites.csv")

head(SiteX)

source("math156//r-homework//calendarHeat.ry")

source("lib/calendarHeat.R")
site_x['singleDate'] <- as.Date(as.POSIXct(site_x$timestamp, origin="1970-01-01"))
calendarHeat(dates=site_x$singleDate,
             values=as.vector(site_x$value),
             color="r2gr", 
             varname="2012 Energy Usage (Site 51)",
             main="h")

calendarHeat(dates=dates,
             values=as.vector(deltaCounts),
             color="r2gr")

Sites;
head(Sites)
AllSites;
head(AllSites)
tail(AllSites)
site_indicies = Sites$SITE_ID; head(site_indicies)
means.again = numeric(length(site_indicies))
for(i in 1:length(site_indicies)) {
  site_subset = AllSites[AllSites$site_id == site_indicies[i],]; head(site_subset)
  means.again[i] = mean(site_subset$value) 
} 

write.csv(Sites, file="math156/r-homework/csv/ModifiedSites.csv")

# Manipulate the means here
mean_per_industry = function(industry) {
  industry = "Education"
  industry_sites = Sites[Sites$INDUSTRY==industry,]
  nrow(industry_sites)
  indexes = industry_sites$SITE_ID
  allIndustrySites = AllSites[is.element(AllSites$site_id, indexes),]
  N = length(indexes)
  site_means = numeric(N)
  for(j in 1:N) {
    site_value = allIndustrySites[allIndustrySites$site_id == indexes[j],]$value  
    site_means[j] = mean(site_value)
  }
  hist(allIndustrySites$value, breaks="fd", main=industry)
  return(site_means/industry_sites$SQ_FT)
}

mean_per_industry("Food Sales & Storage")
mean_per_industry("Commercial Property")
means = mean_per_industry("Education")
means
hist(means, breaks="fd")
mean_per_industry("Light Industrial")

industry = "Education"
industry_sites = Sites[Sites$INDUSTRY==industry,]
site_indicies = industry_sites$SITE_ID
for(i in 1:2) {
  site_id = site_indicies[i] 
  values = AllSites[AllSites$site_id == site_id,]$values
  print(head(values))
}

# --- # 
# # # # 

# Math E-156 Script 11D-Bootstrap-t.R
# Topic 1 -- bootstrapping the Student t statistic
# For a skewed distribution we do not want to assume the sampling distribution 
# is normal. We use the t statistic - sqrt(n)(X - mu)/S but estimate its 
# distribution by bootstrapping
# The book's example 7.21 - arsenic in Bangladesh wells


# Do just for Education
site_means = numeric(25)
for(i in 1:25){
  site_values = AllSites[AllSites$site_id == site_indicies[i],]$value
  site_means[i] = mean(site_values)    
}
all_education_sites = AllSites[is.element(AllSites$site_id, site_indicies),]$value
all_education_sites = site_means
length(site_means)
hist(all_education_sites)
#hist(Arsenic)                #long, flat tail tothe right
#xbar <- mean(Arsenic); xbar  #sample mean
xbar <- mean(all_education_sites); xbar  #sample mean
#S <- sd(Arsenic); S          #sample standard deviation
S <- sd(all_education_sites); S
#n <- length(Arsenic)
n <- length(all_education_sites); n
SE <- S/(sqrt(n)) ; SE       #sample standard error

#Check our methodology with a single bootstrap resample
x <-sample(all_education_sites, size = n, replace = TRUE) #resample
Tstar<-(mean(x) - xbar)/(sd(x)/sqrt(n)); Tstar #a t statistic except the assumption of the underlying dist is not normal
#Now we will estimate the distribution of the t statistic

N = 10000; Tstar = numeric(N) #vector of t statistics
means = numeric(N); 
StdErrs = numeric(N)
hun = 1000
ten = 100
hunMeans = numeric(N)
hunStd = numeric(N)
tenMeans = numeric(N)
tenStd = numeric(N)
for (i in 1:N) {
  x <-sample(all_education_sites, size = n, replace = TRUE)
  Tstar[i] <-(mean(x) - xbar)/(sd(x)/sqrt(n))
  # quotient of student t
  means[i] = mean(x); 
  StdErrs[i] = sd(x)/sqrt(n);
  if(i <= 100) {
    tenMeans[i] = mean(x);   
    tenStd[i] = sd(x)/sqrt(n);  
  }  
  if(i <= 1000) {
    hunMeans[i] = mean(x);   
    hunStd[i] = sd(x)/sqrt(n);  
  }
}

# For this bootstrap sampling distibution none of the usual assumptions are valid
# The standard error is not independent of the mean
# A high mean = high std dev because of really bad wells
plot(means, StdErrs)
points(hunMeans, hunStd, col="red")
points(tenMeans, tenStd, col="green")
#The correlation looks high, and it is!
cor(means, StdErrs)
cor(hunMeans, hunStd)
cor(tenMeans, tenStd)

# The bootstrap t statistic is not even approximately normal or Student t
qqnorm(Tstar)
qqline(Tstar)

# The bootstrap t statistic is skewed to the left, not to the right!
# This is a common phenomenon. It is skewed in the opposite direction
hist(Tstar, breaks = "FD",probability  = TRUE)
# A Student t curve matches the mean and variance but nothing else!
# But we can't fix the mean so we just need to capture the features of the skewness.
curve(dt(x,n-1), col = "red", add = TRUE) 

# The bootstrap quantiles are very different from the t quantiles
q<-quantile(Tstar, c(.025, .975), names = FALSE); q # will differ slightly from page 198 because it is random and depend on initial conditions
qStud <- qt(c(.025, .975), n-1); qStud

# To get a confidence interval, use the bootstrap quantiles 
# along with the sample mean and standard deviation
L <- xbar - q[2]*SE; 
U <- xbar - q[1]*SE; 
L; U

# Here, for comparison, is the bootstrap percentile confidence interval
quantile(means, c(.025, .975))

# Here is the confidence interval based on the assumption of a Student t distribution
xbar - qStud[2]*SE; 
xbar - qStud[1]*SE
# Out of all of them, q using the generic t-statistic is probably the best (most convincing)
# in dealing with real-world skewed data.

# Topic 2 - checking the bootstrap t confidence interval by simulation

# We can do a simulation by pretending that the observed data are the population
counter <- 0; N = 100; badL <- 0; badU <- 0
plot(x =c(50, 200), y = c(1,100), type = "n", xlab = "", ylab = "") #blank plot
for (i in 1:N) {
  x <-sample(all_education_sites, size = n, replace = TRUE)
  Tstar[i] <-(mean(x) - xbar)/(sd(x)/sqrt(n))
  # Calculate a confidence interval using the results from this sample
  L = mean(x)- q[2]* sd(x)/sqrt(n)
  U = mean(x)- q[1]* sd(x)/sqrt(n)
  if (L < xbar && U > xbar) counter <- counter + 1 #count +1 if we were correct
  if (L > xbar) badL <- badL + 1  #lower limit was wrong
  if (U < xbar) badU <- badU + 1  #upper limit was wrong
  if(i <= 100){
    segments(L, i, U, i)
    points(mean(x), i, col = "red", pch = ".", cex = 4) 
  }
}
abline (v = xbar, col = "red") # vertical line at true mean
counter/N #what fraction of the time did our confidence interval include the true mean
# In spite of the skewed data, this confidence interval misses equally on either side
badL/N    #what fraction of the time was L too large?
badU/N    #what fraction of the time was U too small?



