#!/usr/bin/Rscript

###########################
# Running the script:
# > Rscript HW3-E156-FukYeung.R
#
# Sample output is provided.
#
# Set N globally because I swap laptops 
fastComputer <- 100000
slowComputer <- 100
N <- fastComputer
###########################
# For debugging 
options(error=traceback) 
###########################

# #############################
cat("\n*** Problem 1 ***
    
    Textbook Exercise 10: 

    According to the 2000 census, 28.6% of the US adult population 
    received a high school diploma. In a random sample of 800 US adults, 
    what is the probability that between 220 and 230 people have a high
    school diploma. 

    Use a CLT approximation with continuity correction and compare to the 
    exact probability using pbinom().
    \n")

expected = .286 * 800
SE = (800*.286*(1-0.286))^(0.5)

densityA <- (220 - expected)/SE
densityB <- (230 - expected)/SE
probApprox <- pnorm(c(densityA, densityB))

cat("
    Using the CLT approximation we get the estimated probability:",
    probApprox[2] - probApprox[1])

densityA <- (220.5 - expected)/SE
densityB <- (230.5 - expected)/SE
probApprox <- pnorm(c(densityA, densityB))

cat("
    Adding in a continuity correction, we get a better estimate:",
    probApprox[2] - probApprox[1])

probBinom <- pbinom(c(220, 230), 800, .286)
cat("
    Using pbinom(), the exact probability is:", probBinom[2] - probBinom[1])

# #############################
cat("\n\n*** Problem 2 ***

    Textbook problem 14:

    Let X_1, X_2, ... X_9 ~ N(7, 3^2) and
    let Y_1, Y_2, ... Y_12 ~ N(10, 5^2).
    Let W = X.hat - Y.hat

  a) Give the exact sampling distribution of W.
    ")

mean <- 7-10 
var <- ((3^2)/9) + ((5^2)/12)
SE <- (var)^(0.5)

cat("
    Using Theorem A.10, the mean is just the difference of the mean:", mean, "
    The variance is the sum of the variances divided by the number of samples:", var, " 
    Finally, the SE is just the square root of the variance:", SE, "
    This gives us a normal random variable with W ~ N(", mean, ",", SE, "^2)")

pdf <- function(x) {
    return(dnorm(x, mean, SE))
}

W <- numeric(N)
for (i in 1:N) {
    x <- rnorm(9, 7, 3)
    y <- rnorm(12, 10, 5)
    W[i] <- mean(x) - mean(y)
}
hist(W, main="Problem 2: W=X.hat-Y.hat", breaks="FD", freq=F, probability=T)
curve(pdf, col="blue", add=TRUE)
legend(-1,.23, c("dnorm(x, -3, 1.76)"),  lwd=c(2.5),col=c("blue"))

cat("\n
  b) We can simulate this sample using a for loop running", N, "times.

    Doing this, the simulated mean is", mean(W), "
    The simulated standard error is", sd(W), "
    These are actually pretty close, about a 1% difference.
    
  c) We can use this simulation run to find P(W < -5)") 

estimatedProb <- (sum(W < -1.5) + 1)/(N+1)
actualProb <- pnorm(c(-1.5), mean(W), sd(W))

cat("\n
    Doing this, the simulated probability is", estimatedProb, "
    Using pnorm, the theoretical probability is", actualProb, "
    Again, this is in pretty good agreement.") 

# #############################
cat("\n\n*** Problem 3 ***

    Textbook problem 16:

    Let X be a uniform random variable on the interval [40, 60] and Y a 
    uniform random variable on [45, 80]. X and Y are independent.

  a) Compute the expected value and variance of X + Y.  

    From Theorem B.7, given X~unif[a,b]
      E[X] = (a+b)/2
      Var[X] = (a-b)^2/12

    ")

# NOTE:
# I'm actually not sure if I believe this case.
expectedX <- (60 + 40)/2
expectedY <- (80 + 45)/2
sumExp <- expectedY + expectedX

expectedVarX <- (60 - 40)^2/12
expectedVarY <- (80 - 45)^2/12
sumExpVar <- expectedVarY + expectedVarX

cat("
    By the linearity of expectation, the expected value is the sum of 
    the individual expected values:", sumExp, "
    Because the random variables are independent, the variance is also 
    linear and the sum of the variances:", sumExpVar, "

  b) We can simulate the sampling distribution using the code 
    provided in the textbook.")

# This code looks like it skips the loop altogether and just 
# adds x and y pairwise so that the length of the total is 1000
X <- runif(N, 40, 60)  
Y <- runif(N, 45, 80)
total <- X + Y

hist(total, breaks="fd", main="Problem 3: Distribution of X + Y", probability=T)
#expSum <- function(x)  {dnorm(x, sumExp, sumExpVar^(.5))}
#expSumPlot <- Vectorize(function(x) expSum (x))
#curve(expSumPlot, lwd=c(2.5,2.5), col="BLUE", add=T)
#legend(.03,100, c("exp(rate=n*lambda)"),  lwd=c(2.5),col=c("blue"))

cat("\n
    Using the code, the simulated mean is:", mean(total), "
    The simulated variance is:", var(total))

cat("\n
  c) We want to find the likelihood that the sum of X and Y is 
    less than 90. To do this, we can simulate a sampling distribution
    and count the number of times the result is less than 90.")

probTotal <- (sum(total < 90) + 1)/(N+1)

cat("\n
    Using N=", N, ", we find the probability to be", probTotal, "
    This is very small and the likelihood is not very high. This makes 
    intuitive sense because the interval minimums add up to 85 and the 
    maximums add up to 140.")

###############################################################################

cat("\n*** Problem 4 *** 

    Textbook problem 24 
    
  a) Let X_1, X_2, ..., X_n ~ Exp(lambda) with pdf f(x) = lambda exp(-lambda x)
    with lambda > 0, x > 0.
    
    Find the pdf f_min(x) for the sample minimum X_min. Recognize this as 
    the pdf of a known distribution.

    From the textbook:
    
    (1)    f_min(x) = n(1-F(x))^(n-1) f(x)

    Using:
    (2)    f(x) = lambda exp(-lambda x)
    (3)    F(x) = 1 - exp(-lambda x)

    Plugging (2) and (3) into (1), We can reduce:

    (4)    f_min(x) = n lambda exp(-n lambda x)

    This is something we can recognize as another exponential distribution
    but with the parameter (n*lambda).

  b) Now, simulate the sampling distribution of X_min of samples of size
    n = 25 from the exponential distribution with lambda = 7. ")

theoretical <- 1/(25*7)
minimums = numeric(N)
for (i in 1:N) {
    sample <- rexp(25, rate=7)
    minimums[i] <- min(sample)
}

hist(minimums, breaks="fd", main="Problem 4: Distribution of Minimums", probability=T)
expSum <- function(x)  {dexp(x, rate=25*7)}
expSumPlot <- Vectorize(function(x) expSum (x))
# Plot the exponential distribution that we predicted
curve(expSumPlot, lwd=c(2.5,2.5), col="BLUE", add=T)
legend(.03,100, c("exp(rate=n*lambda)"),  lwd=c(2.5),col=c("blue"))

cat("\n
    Since our theoretical distribution is just an exponential, the 
    theoretical expected value is 1/lambda or ", theoretical, "
    Using the values of n and lambda in a for loop, we can use 
    rexp() to simulate an expected value of X_min:", mean(minimums))

###############################################################################

cat("\n\n*** Problem 5 *** 

    Plotting the moment-generating function:

  a) Using the outer() to get the probability mass function for 2 dice.
    Then, we can write a mgf and plot it.
    
    The plot is attached as a pdf.")

mgf <- function(x, Y, pr)  {
    sum(pr*(exp(x*Y)))
}

diceSums <- outer(1:6, 1:6, "+")
diceProbs <- outer(1:6, 1:6, "*")

mgfPrPlot <- Vectorize(function(x) mgf (x, diceSums, diceProbs))

cat("\n
  b) Write an R function for the moment generating function of the 
    probability mass function for a single die and plot a graph of its square.

    The plot is attached as a pdf.
    ")

# The moment generating function does not change
mgfSq <- function(x, Y, pr)  {
    sum(pr*(exp(x*Y)))
}

# We don't need to use outer here since its a single vector
oneDiceSums <- c(1:6)
oneDiceProbs <- rep(c(1.0/6.0), 6)

# First Plot the square of the single die
mgfSqPlot <- Vectorize(function(x) (mgfSq (x, oneDiceSums, oneDiceProbs)^2)) 
curve(mgfSqPlot, 
      lwd=c(2.5,2.5), 
      from=0, 
      to=2, 
      col="RED", 
      main="Problem 5: MGF for Dice")

# Then add the double dice
curve(mgfPrPlot, 
      lwd=c(2.5,2.5), 
      col="BLUE", 
      add=T)
legend(.5,1000000000, c("1 Dice","2 Dice"),  lwd=c(2.5,2.5),col=c("red","blue"))
