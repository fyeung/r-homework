

*** Problem 1 ***

    Textbook Exercise 17.

    a) Create a random variable X that has a chi-square distribution with six degrees
        of freedom by sampling from an exponential distribution. 
    b) Create a variable with an approximate Student t distribution with 
        six degrees of freedom by taking the mean of 50 samples from Unif[0,1] and dividing
        by the chi-square variable in part a)



*** Problem 2 ***

    By evaluating the gamma functions in the t distribution density,
    find the formulas for df=4,5 and show that they agree with dt(x).
    Using 50000 trials, confirm that the variance is close to k/(k-2) and
    attempt to estimate the fourth moment in each case.
    [1] 2
[1] 2.00939
[1] 1.666667
[1] 1.675268
[1] 69.40375
[1] 42.26232


*** Problem 3 ***

  The only distribution for which the sample mean and the sample variance are 
  independent is the normal distribution.

  For samples of size 6 from Unif[-1,1] create scatter plots of the square of the sample
  mean against the sample variance and calculate the correlation.
  Offer an anecdotal explanation based on an extreme case.
[1] 0.1913708
[1] -0.3169563

*** Problem 4 *** 

    Create samples of size 6 of the random variable X=(Y-mu)/theta

    (a) If you multiply the square of the sample mean by n, does it have a 
        distribution that is approximately chi-square with k=1 degree of freedom?

    (b) Does the sum of the squares of the samples have a distribution that is 
        approximately chi-square with k=6 degrees of freedom?
    
    (c) If you multiply the sample variance by k-1, does it have a distribution
        that is approximately chi-square with k-1=5 df?

    (d) Is the square of the sample mean uncorrelated with the sample variance?

    (e) If you divide the sample mean by the sample standard deviation, does it 
        have an approximate Student t distribution with k-1=5 df?
    
  ID MothersAge Smokers Alcohol Gender Weight Gestation
1  1      30-34      No      No   Male   3827        40
2  2      30-34      No      No   Male   3629        38
3  3      35-39      No      No Female   3062        37
4  4      20-24      No      No Female   3430        39
5  5      25-29      No      No   Male   3827        38
6  6      35-39      No      No Female   3119        39
[1] -0.03053127
[1] -0.03053127

*** Problem 5 *** 

    Textbook Exercise 6.

    Julie is interested in the suger content of vanilla ice cream. She obtains a random sample
    of n=20 brands and finds an average of 18.05 g with standard deviation 5 g. Assuming that 
    the data come from a normal distribution, find a 90% confidence interval for the mean 
    amount of sugar in a half cup serving of vanilla ice cream.
    
[1] 16.211
[1] 19.889
[1] 0.9

*** Problem 6 *** 

    Textbook Exercise 19.

    As many states,Tennessee conducts audits of stores to determine whether or 
    not proper sales tax was assessed. The Department of Revenue obtains a random
    sample of transactions at the audited store and for each transaction looks at 
    the tax error de???ned to be the amount of tax owed minus the amount of tax paid.
    The auditors examine the lower bound of a 75% one-sided upper t con???dence interval
    and if it is larger than 0, the store owes the state money 
    (www.tn.gov/revenue/tntaxes/sales/statsamplingapr06.pdf).

    Suppose the Department of Revenue samples 500 transactions of a certain store
    and ???nds the average tax error to be $5.29 with a standard deviation of $3.52.
    Compute a 75% one-sided upper t con???dence interval for the true mean tax error.
    
[1] 87.5
[1] -0.6749807
[1] 5.183745
[1] 2.040816
[1] 2.45
[1] 5.001119
[1] 3.492446
[1] 0.181
