
*** Problem 1 *** 
    
    Textbook Exercise 12

    Consider the data set Girls2004 (see Case Study in Section 1.2).

    (a) Create exploratory plots and compare the distribution of weights between babies born 
        to nonsmokers and babies born to smokers.

    (b) Find a 95% one-sided lower t confidence bound for the mean difference in weights between 
        babies born to nonsmokers and smokers. Give a sentence interpreting the interval.

    (c) As in exercise 11, find a 95% t confidence interval for the mean difference in weights 
        between babies born to nonsmokers and smokers. 
    
  ID State MothersAge Smoker Weight Gestation
1  1    WY      15-19     No   3085        40
2  2    WY      35-39     No   3515        39
3  3    WY      25-29     No   3775        40
4  4    WY      20-24     No   3265        39
5  5    WY      25-29     No   2970        40
6  6    WY      20-24     No   2850        38
   ID State MothersAge Smoker Weight Gestation
75 75    AK      30-34     No   3572        40
76 76    AK      35-39     No   3968        39
77 77    AK      20-24     No   4564        42
78 78    AK      20-24     No   4210        40
79 79    AK      25-29     No   3260        38
80 80    AK      20-24     No   3600        40
[1] 2212
[1] 3995
[1] 2182
[1] 4592
[1] -308.45
[1] -529.7695
[1] -87.13051
[1] 2182
[1] 4592
[1] 2352
[1] 3923
[1] -286.9433
[1] -32.53422
[1] 1.959964
[1] -590.0905
[1] 16.20382

*** Problem 2 *** 
    
    Textbook Exercise 20:
    
    One question in the 2002 General Social Survey asked participants whom 
    they voted for in the 2000 election. Of the 980 women who voted, 459 voted for Bush. 
    Of the 759 men who voted, 426 voted for Bush.

    (a) Find a 95% confidence interval for the proportion of women who voted for Bush.

    (b) Find a 95% confidence interval for the proportion of men who voted for Bush. 
        Do the intervals for the men and women overlap? What, if anything, can you 
        conclude about gender difference in voter preference?

    (c) Find a 95% confidence interval for the difference in proportions and 
        interpret your interval.

    Addendum:

    (d) After solving the problem, make a dataframe to simulate the survey results. 
        Do a permutation test to see whether there is gender difference in voter preference, 
        and construct bootstrap t-confidence intervals to compare with your 
        answers to (a), (b), and (c).
    
        
          No Yes
  Female 521 459
  Male   333 426
[1] 426
[1] 0.5563727 0.6228136
[1] 980
[1] 0.4683673
[1] 0.4371872 0.4997945
[1] 759
[1] 0.5612648
[1] 0.5255620 0.5963506
[1] 0.04575569 0.14003926
attr(,"conf.level")
[1] 0.95
   97.5% 
1.958688 
[1] 0.5255862 0.5963272
   97.5% 
1.898715 
[1] 0.4381621 0.4988045
   97.5% 
1.924771 
[1] 0.5262123 0.5957222
   97.5% 
1.852757 
[1] 0.4388931 0.4980625


*** Problem 3 ***

    Textbook Exercise 36.

    In this exercise, you will derive and use a ???bootstrap Z??? interval.

    (a) Following the steps in the derivation of the bootstrap t interval 
        in Section 7.5, derive a bootstrap Z interval for mu, for cases 
        when s is known.

    (b) Calculate this interval for the Verizon CLEC data; 
        for s, use the sample variance of the Verizon ILEC data. 
        (In practice, we may use methods for known s when we can estimate s 
        from a large related data set.)

    (c) Compare that interval with a formula z interval. How does the bootstrap Z 
        interval adjust for skewness?
      Time Group
1665 26.62  CLEC
1666  8.60  CLEC
1667  0.00  CLEC
1668 21.15  CLEC
1669  8.33  CLEC
1670 20.28  CLEC
[1] 14.69004
[1] 23
[1] 16.50913
      2.5%      97.5% 
-0.4239815  0.5977609 
[1] 7.728
[1] 22.73743
[1] -1.959964  1.959964
[1] -12.28282
[1] 45.30108


*** Problem 4 ***

    a) Textbook Exercise 38:

    Robin wonders how much variation there is in a box of his favorite cereal. 
    He buys eight boxes from eight different stores and finds the weights of the 
    contents to be (in grams):
      560 568 580 550 581 581 562 550

    Assuming the data are from a normal distribution, find a 90% confidence 
    interval for the variance s2. 
    (In R, the command qchisq computes quantiles for the chi-square distribution.)

    b) Do a simulation where the weights of the eight cereal boxes are drawn
       from N(560; 102), and check that, when you calculate the confidence
       interval as in part (a), the true variance of 100 falls within the  
       confidence interval in roughly 95% of the cases.
    [1]  2.16735 14.06714
[1] 2.16735
[1] 14.06714
[1] 173.1429
[1] 86.15824
[1] 559.2083
[1]  1.689869 16.012764
[1] 1.689869
[1] 16.01276
[1] 0.958


*** Problem 5 ***

    Suppose that you draw two samples from N(mu, theta) with mu=5, theta=2
    In this case, the quantity T = sqrt(2)*(X.bar - mu)/S is a pivotal statistic, 
    since its distribution (Student t with one degree of freedom, a.k.a. Cauchy) 
    does not depend on the unknown parameter. You can therefore calculate from
    the sample mean X.bar two quantities L and U such that the events mu < L,
    L <= mu <= U, and U < mu all have probability 1/3. Do a simulation with
    3000 trials to show that this idea works. You can get the required quantiles
    either from qcauchy() or from qt().
[1] 0.3386667
[1] 0.3263333
[1] 0.335
